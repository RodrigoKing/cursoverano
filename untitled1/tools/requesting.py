import requests
import webbrowser
from bs4 import BeautifulSoup
from requests.exceptions import ConnectionError

#user keeps keeps logged in need get refresh user session method
#PASSWORD LIMIED TO 10 CHARACTERS 1L0v3pytho

def search_subject_data(search_term, html_children, j):
    for i, td in enumerate(html_children.find_all("td")):
        if search_term in td.text:
            return html_children.find_all("td")[i], html_children.find_all("td")[i + j]
    return None
        


def sie_logout(matricula, password):
    return requests.get("http://189.240.193.237/cgi-bin/sie.pl?Opc=CSESIONALU&amp;Control="+matricula+"&amp;Password="+password+"&amp;dummy=0")

#matricula = "16530797"
#password = "1L0v3pytho"
#option = "KARDEX"

def query_SIE_kardex(matricula, password, search_term):
    url = "http://189.240.193.237/cgi-bin/sie.pl"
    payload = {"aceptar":"Aceptar",
		 "Control": matricula,
		 "Opc": "MAIN",
		 "password": password }


    option = "KARDEX"
    try:
        requests.get("http://189.240.193.237/intertec")
        r = requests.post(url, data=payload)
        response = str(r.content)
    except ConnectionError as e:
        response = None

    if response:
        try:
            key = response.split("Password=")[1].split("&dummy=0")[0]
        except:
            return "ServerError"			

        kardex = requests.get("http://189.240.193.237/cgi-bin/sie.pl?Opc="+ option+"&Control=" 
					+ matricula + "&Password=" + key + "&dummy=0" )

        soup = BeautifulSoup(kardex.content, "html.parser")
        data = list(soup.children)[2]

        response = search_subject_data(search_term, data, 2)
        if response:
            subj = response[0].text
            calif = response[1].text

            sie_logout(matricula, key)
		

            return subj, calif
        else:
            sie_logout(matricula, key)
            return "NotFound"
    else:
        return "ConectionError"



def query_SIE_calificaciones(matricula, password, search_term):
    url = "http://189.240.193.237/cgi-bin/sie.pl"
    payload = {"aceptar":"Aceptar",
		 "Control": matricula,
		 "Opc": "MAIN",
		 "password": password }


    option = "CALIF"
    try:
        requests.get("http://189.240.193.237/intertec")
        r = requests.post(url, data=payload)
        response = str(r.content)
    except ConnectionError as e:
        response = None

    if response:
        try:
            key = response.split("Password=")[1].split("&dummy=0")[0]
        except:
            return "ServerError"			

        calif = requests.get("http://189.240.193.237/cgi-bin/sie.pl?Opc="+ option+"&Control=" 
					+ matricula + "&Password=" + key + "&dummy=0" )

        soup = BeautifulSoup(calif.content, "html.parser")
        data = list(soup.children)[0]

        response = search_subject_data("ACA0910G", data,3)
    
        if response:
            subj = response[0].text
            calif = response[1].text

            sie_logout(matricula, key)
		

            return subj, calif
        else:
            sie_logout(matricula, key)
            return "NotFound"
    else:
        return "ConectionError"

def query_SIE(matricula, password):
    url = "http://189.240.193.237/cgi-bin/sie.pl"
    payload = {"aceptar": "Aceptar",
               "Control": matricula,
               "Opc": "MAIN",
               "password": password}

    try:
        requests.get("http://189.240.193.237/intertec")
        r = requests.post(url, data=payload)
        response = str(r.content)
    except ConnectionError as e:
        response = None

    if response:
        try:
            key = response.split("Password=")[1].split("&dummy=0")[0]
            return True, key
        except:
            return False, "ServerError"

def query_SIE_data(matricula, key):
    option = "DATOSGRALES"
    data = requests.get("http://189.240.193.237/cgi-bin/sie.pl?Opc=" + option + "&Control="
                 + matricula + "&Password=" + key + "&dummy=0")
    formated_data = str(data.content).replace("\\n", "").replace("\\r", "").replace("<TD>", "").replace("</TD>", "")
    name = formated_data.split("Nombre del Alumno")[1].split("</TR>")[0].strip()
    carrera = formated_data.split("Carrera")[1].split("</TR>")[0].strip()

    sie_logout(matricula, key)
    return name, carrera


            




