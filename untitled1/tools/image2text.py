import cv2
import numpy as np
import pytesseract
from PIL import Image


src_pth = "/home/laptop/Pictures/"

def get_string(img_path):
    img = cv2.imread(img_path)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    #kernel = np.ones((1,1), np.uint8)
    #img = cv2.dilate(img, kernel, iterations=1)
    #img = cv2.erode(img, kernel, iterations=1)

    cv2.imwrite(src_pth + "removed_noise.png", img)
    #img = cv2.threshold(img, 0, 255,
    #		cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
    img = cv2.medianBlur(img, 3)

    cv2.imwrite(src_pth + "thres.png", img)

    result = pytesseract.image_to_string(Image.open(src_pth + "thres.png"))
    
    if result:
        return result
    else:
        return "what happened?"
    
    

r1 = get_string(src_pth + "oxxo_pago.png")
print(r1)

