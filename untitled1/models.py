from untitled1 import db, login_manager
from flask_login import UserMixin
from untitled1 import app
from itsdangerous import TimedJSONWebSignatureSerializer as Cerealizer


@login_manager.user_loader
def load_user(user_id):
    return user.query.get(int(user_id))

"""
inscriptions = db.Table("inscriptions",
                        db.Column("user_id", db.Integer, db.ForeignKey("user.id")),
                        db.Column("course_id", db.Integer, db.ForeignKey("course.id")),
                        db.UniqueConstraint('user_id', 'course_id', name='UC_location_id_headings_id')
                        )
"""

teacher_courses_table =  db.Table("teacher_courses",
                                  db.Column("user_id", db.Integer, db.ForeignKey("user.id")),
                                  db.Column("available_courses_id", db.Integer, db.ForeignKey("available_courses.id")))


teacher_created_courses_table =  db.Table("teacher_created_courses",
                                  db.Column("course_id", db.Integer, db.ForeignKey("course.id")),
                                  db.Column("user_id", db.Integer, db.ForeignKey("user.id"))
                                  )


class course(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(30), unique=True, nullable=False)
    key = db.Column(db.String(10), unique=True, nullable=False)
    requirement = db.Column(db.String(10), unique = False, nullable = True)
    teacher = db.Column(db.String(30), unique=False, nullable=False, default="Por definir")
    hour = db.Column(db.String(30), unique=False, nullable=False, default="Por definir")
    students = db.relationship("user", secondary="user_course_link", lazy = "dynamic")
    possible_teachers = db.relationship("user", secondary = teacher_created_courses_table)


#ADD TEACHER TO COURSES REL
class user(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key = True)
    matricula = db.Column(db.String(10), unique=True, nullable=False)
    password = db.Column(db.String(30), nullable=False)
    key = db.Column(db.String(30), nullable=False)
    courses = db.relationship("course", secondary = "user_course_link", lazy = "dynamic")
    offered_courses = db.relationship("available_courses", secondary=teacher_courses_table)
    role = db.Column(db.Integer, unique=False)


    """

    def reset_token(self, expiration=3600):
        cereal = Cerealizer(app.config["SECRET_KEY"], expiration)
        return cereal.dumps({"user_id": self.id}).decode("utf-8")

    @staticmethod
    def verify_reset_token(token):
        cereal = Cerealizer(app.config["SECRET_KEY"])

        try:
            user_id = cereal.loads(token)["user_id"]
        except:
            return None

        return user.query.get(user_id)

    """


    def __repr__(self):
        return f'{self.matricula}'






class UserCourseLink(db.Model):
    __tablename__ = "user_course_link"
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"), primary_key = True)
    course_id = db.Column(db.Integer, db.ForeignKey("course.id"), primary_key = True)
    inscription_time = db.Column(db.Integer, unique=False)
    User  = db.relationship("user", backref=db.backref("course_rel"))
    Course = db.relationship("course", backref=db.backref("Students"))
    account = db.Column(db.String(15), unique=False, nullable=False, default="Sin pagar")
    ammount = db.Column(db.Integer, unique=False, nullable=False, default="Sin pagar")
    date = db.Column(db.String(20), unique=False, nullable=False, default="Sin pagar")
    Authorization = db.Column(db.String(20), unique=False, nullable=False, default="Sin pagar")
    payment_image = db.Column(db.String(20), nullable=True)








    def __repr__(self):
        return f"{self.name}"

class available_courses(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    key = db.Column(db.String(10), unique=True, nullable = False)
    name = db.Column(db.String(30), unique=True, nullable=False)
    requirement = db.Column(db.String(10), unique = False)

    def __repr__(self):
        return f"{self.name}"
