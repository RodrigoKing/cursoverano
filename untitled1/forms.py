from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, BooleanField, SelectField, FileField, RadioField
from wtforms.validators import DataRequired, Length, Email, EqualTo, ValidationError
from wtforms.ext.sqlalchemy.fields import QuerySelectField
from untitled1.models import user, available_courses, course
from untitled1 import db
from flask_wtf.file import FileAllowed
from flask_login import current_user
from string import ascii_lowercase


def query_courses():
    return db.session.query(available_courses).all()





def validate_emaildomain(form, field):
    domain = field.data.split("@")[1]
    if domain != "itcancun.edu.mx":
        raise ValidationError("Solo se puede usar el correo de la institucion.")

class RegistrationForm(FlaskForm):
    matricula = StringField("Matricula", validators = [DataRequired()])
    #email = StringField('Email', validators=[DataRequired(), Email(), validate_emaildomain])
    sie_password = StringField("SIE password")
    password = PasswordField('Password', validators=[DataRequired()])
    confirm_password = PasswordField('Confirmar Password', validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Registrarse')

    def validate_matricula(self, matricula):
        User = user.query.filter_by(matricula=matricula.data).first()
        if User:
            raise ValidationError("Esta matricula ya esta en uso")

class LoginForm(FlaskForm):
    matricula = StringField('Matricula', validators=[DataRequired()])
    password = PasswordField('Password')
    remember = BooleanField('Remember Me')
    submit = SubmitField('Ingresar')


class CourseForm(FlaskForm):
    name = QuerySelectField(u'Courses', validators=[DataRequired()], query_factory=query_courses)
    submit = SubmitField("Crear")

    def validate_name(self, name):
        Course = course.query.filter_by(name=name.data.name).first()
        try:
            if len(list(Course.students)) <= 25:
                raise ValidationError("Ya existe hay un curso abierto de ")
        except:
            pass

class TeacherCourseForm(FlaskForm):
    name = QuerySelectField(u'Courses', validators=[DataRequired()], query_factory=query_courses)
    submit = SubmitField("Crear")





class InscriptionForm(FlaskForm):
    key = PasswordField("Clave", validators=[DataRequired()])
    submit = SubmitField("Inscribirse")

class PaymentForm(FlaskForm):
    image = FileField("Foto de pago", validators = [FileAllowed(["jpg", "png"])])
    submit = SubmitField("Pagar")

class RequestResetForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired(), Email()])
    submit = SubmitField("Recuperar contraseña")

    def validate_email(self, email):
        User = user.query.filter_by(email=email.data).first()
        if User is None:
            raise ValidationError("No existe una cuenta con ese correo")

class ResetPasswordForm(FlaskForm):
    password = PasswordField('Password', validators=[DataRequired()])
    confirm_password = PasswordField('Confirmar Password', validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Cambiar contraseña')

class RegisterConfirmation(FlaskForm):
    key = PasswordField('Clave personal', validators=[DataRequired()])
    confirm_key = PasswordField('Confirmar clave', validators=[DataRequired(), EqualTo('key')])
    submit = SubmitField("Aceptar")
