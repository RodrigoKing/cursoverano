from flask import render_template, url_for, flash, redirect, request, session, json
from untitled1.forms import RegistrationForm, LoginForm, CourseForm, InscriptionForm, PaymentForm, RequestResetForm, ResetPasswordForm, TeacherCourseForm, RegisterConfirmation
from untitled1 import app, db, bcrypt, mail
from untitled1.models import user, course, available_courses
from flask_login import login_user, current_user, logout_user, login_required
from untitled1.tools.requesting import query_SIE, query_SIE_data
import os
from flask_mail import Message




########################################################### GENERIC ########################################################################################

@app.route("/home", methods=["GET","POST"])
@app.route("/", methods=["GET","POST"])
@app.route("/login", methods=["GET","POST"])
def login():
    if current_user.is_authenticated:
        return redirect(url_for("students_home"))
    form = LoginForm()
    if form.validate_on_submit():
        matricula = form.matricula.data
        password = form.password.data[:10]
        User = user.query.filter_by(matricula=matricula).first()
        if User:
            if password == User.password:
                login_user(User, remember=form.remember.data)
            else:
                flash("Error, verificar matricula o contraseña", "warning")
        else:
            for i in range(5):
                query_state, response_key = query_SIE(matricula, password) #LIMIT TO 10 CHARACTERS
            if query_state:
                name, carrera = query_SIE_data(matricula, response_key)
                session["name"] = name
                session["carrera"] = carrera
                session["matricula"] = matricula
                session["pwd"] = password
                return redirect(url_for("agreement"))
            else:
                flash("Error de SIE , verificar matricula o contraseña", "warning")


        """
        if len(matricula) > 4:
            User = user.query.filter_by(matricula=matricula).first()
            role = "student"
        else:
            User = teacher.query.filter_by(matricula=matricula).first()
            role = "teacher"
        if User and bcrypt.check_password_hash(User.password, form.password.data):
            login_user(User, remember=form.remember.data)
            if role == "student":
                return redirect(url_for("students_home"))
            elif role == "teacher":
                return redirect(url_for("teachers_home"))
            
        else:
            flash("Error, verificar matricula, contraseña o rol", "warning")
        """

    return render_template('login.html', title='Login', form=form)


@app.route("/register", methods=["GET", "POST"])
def register():
    if current_user.is_authenticated:
        return redirect(url_for("students_home"))
    form = RegistrationForm()
    if form.validate_on_submit():
        mat = form.matricula.data
        hash_pw = bcrypt.generate_password_hash(form.password.data).decode("utf-8")
        sie_pw = form.sie_password.data
        if len(mat) > 4:
            username = user(matricula = mat, sie_password = sie_pw, password= hash_pw)
        else:
            username = teacher(matricula=mat, password=hash_pw)
        db.session.add(username)
        db.session.commit()
        flash(f"Cuenta creada para {form.matricula.data}", "success")
        return redirect(url_for("login"))
    return render_template('register.html', title='Register', form=form, current_user = current_user)


@app.route("/recuperar_contraseña", methods=["GET", "POST"])
def reset_request():
    if current_user.is_authenticated:
        return redirect(url_for("home"))
    form = RequestResetForm()
    if form.validate_on_submit():
        User = user.query.filter_by(matricula=form.matricula.data).first()
        try:
            send_email(User)
        except:
            flash("Error del servidor, lo siento", "warning")
            return redirect(url_for("login"))
        flash("Confirmacion de cambio de contraseña enviado a tu correo", "info")
        return redirect(url_for("login"))
    return render_template("reset_request_template.html", title = "Solicitar contraseña", form=form)


@app.route("/RecuperarContraseña/<token>", methods=["GET","POST"])
def reset_password(token):
    if current_user.is_authenticated:
        return redirect(url_for("home"))
    User = user.verify_reset_token(token)
    if User is None:
        flash("Token expirado o invalido", "warning")
        return redirect(url_for("reset_request"))
    form = ResetPasswordForm()
    if form.validate_on_submit():
        hash_pw = bcrypt.generate_password_hash(form.password.data).decode("utf-8")
        User.password = hash_pw
        db.session.commit()
        flash(f"Contraseña cambiada!", "success")
        return redirect(url_for("login"))
    return render_template("reset_password_template.html", title = "Recuperar contraseña", form = form)


@app.route("/logout")
def logout():
    logout_user()
    return redirect(url_for("login"))







########################################################### STUDENTS ########################################################################################

@app.route("/agreement", methods=["GET","POST"])
def agreement():
    form = RegisterConfirmation()

    matricula = session["matricula"]#request.args.get("matricula")
    password = request.args.get("password")
    name = session["name"]#request.args.get("name")
    password = session["pwd"]
    carrera = session["carrera"]
    if form.validate_on_submit():
        hash_key = bcrypt.generate_password_hash(form.key.data).decode("utf-8")
        username = user(matricula=matricula, password=password, key = hash_key)
        db.session.add(username)
        db.session.commit()
        return redirect(url_for("login"))
    return render_template('first_time.html', matricula = matricula, password = password, name = name, carrera = carrera, form = form)


@app.route("/estudiantes", methods=["GET","POST"])
@login_required
def students_home():
    created_courses = course.query.all()
    return render_template('students_home.html', created_courses=created_courses)

@app.route("/course/new", methods=["GET","POST"])
@login_required
def new_course():
    created_courses = course.query.all()
    form = CourseForm()
    if form.validate_on_submit():
        course_key = available_courses.query.filter_by(name = str(form.name.data)).first().key
        course_requirement = available_courses.query.filter_by(name = str(form.name.data)).first().requirement
        NewCourse = course(name=str(form.name.data), key = course_key, requirement = course_requirement)
        db.session.add(NewCourse)
        db.session.commit()
        flash("Curso creado con exito!", "success")
        return redirect(url_for("students_home"))
    if form.errors:
        for error in form.errors:
            if error == "name":
                flash("Este curso ya existe, revisa la base de datos de cursos", 'success')
    return render_template("create_course.html", title="Crear curso", form=form, created_courses=created_courses)


@app.route("/course/<course_id>", methods=["GET","POST"])
@login_required
def course_page(course_id):
    form = InscriptionForm()
    created_courses = course.query.all()
    Course = course.query.get(course_id)
    if Course.students.filter_by(matricula=current_user.matricula).first():
        form.submit.label.text = "Desinscribirse"
        if form.validate_on_submit():
            if bcrypt.check_password_hash(current_user.key, form.key.data):
                Course.students.remove(current_user)
                #db.session.delete(current_user)
                db.session.commit()
                flash("Desinscrito con exito", "warning")
                return redirect(url_for("course_page", course_id=Course.id))
            else:
                flash("Clave incorrecta", "warning")
                return redirect(url_for("course_page", course_id=Course.id))
        return render_template("course_template.html", title = "Course" , course_data = Course, created_courses=created_courses, form = form)
    else:
        if form.validate_on_submit():
            if bcrypt.check_password_hash(current_user.key, form.key.data):
                if len(current_user.course_rel) == 2:
                    flash("Solo puedes tomar 2 cursos en verano", "warning")
                    return redirect(url_for("course_page", course_id=Course.id))
                else:
                    Course.students.append(current_user)
                    db.session.add(Course)
                    db.session.commit()
                    flash("Inscrito con exito", "success")
                    return redirect(url_for("course_page", course_id = Course.id))
            else:
                flash("Clave incorrecta", "warning")
                return redirect(url_for("course_page", course_id=Course.id))
        return render_template("course_template.html", title="Course", course_data=Course,
                               created_courses=created_courses,
                               form=form)




@app.route("/cuenta/<user_id>", methods = ["GET", "POST"])
@login_required
def my_courses(user_id):
    if int(user_id) != current_user.id:
        return redirect(url_for("my_courses", user_id = current_user.id))
    created_courses = course.query.all()
    return render_template("my_courses_template.html", title = "Mis cursos", user_data = current_user, created_courses=created_courses)




def save_image(form_image):
    _, f_ext = os.path.splitext(form_image.filename)
    image_fn = "u" + str(current_user.id) + f_ext
    picture_path = os.path.join(app.root_path, "images/payment_images", image_fn)
    form_image.save(picture_path)

    return image_fn

# MAKE IMPOSSIBLE for other users to visit this, FATAL BUG
@app.route("/cuenta/<user_id>/<course_id>", methods = ["GET", "POST"])
@login_required
def payment_page(user_id, course_id):
    if int(user_id) != current_user.id:
        return redirect(url_for("my_courses", user_id = current_user.id))
    form = PaymentForm()
    created_courses = course.query.all()
    if form.validate_on_submit():
        if form.image.data:
            image_file = save_image(form.image.data)
            current_user.course_rel[int(course_id)].payment_image = image_file

    return render_template("payment_template.html", title = "Pagos", created_courses = created_courses, form = form)
"""
def send_email(userv):
    token = userv.reset_token()
    msg = Message("Solicitud de recuperacion de contraseña", sender="Asistencia@veranosITC.com", recipients=[user.email])
    msg.body = token
    mail.send(msg)
"""

@app.route("/cursos_disponibles", methods=["GET","POST"])
@login_required
def mobile_available_courses():
    created_courses = course.query.all()
    return render_template('available_courses_template.html', created_courses=created_courses)

########################################################### TEACHERS ########################################################################################

@app.route("/profesores", methods = ["GET", "POST"])
@login_required
def teachers_home():
    created_courses = course.query.all()
    form = TeacherCourseForm()
    if form.validate_on_submit():
        teacher_course = form.name.data
        query = course.query.filter_by(name=str(teacher_course)).first()
        if query:
            query.possible_teachers.append(current_user)
            db.session.add(query)
        current_user.offered_courses.append(teacher_course)
        db.session.add(current_user)
        db.session.commit()
        flash(teacher_course.id)
    return render_template("teacher_template.html", title="Profesores", created_courses=created_courses, form = form)


@app.route("/OfrecerCurso")
@login_required
def offer_course():
    created_courses = course.query.all()
    form = CourseForm()
    return render_template("offer_course.html", title="Crear curso", form=form, created_courses=created_courses)

########################################################### ADMIN #############################################################################################

@app.route("/administrativo")
@login_required
def admin():
    created_courses = course.query.all()
    return render_template("admin_template.html", title="Administracion", created_courses = created_courses, form = form)
